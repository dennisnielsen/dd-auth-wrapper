# DD-AUTH-WRAPPER module
## About the module
The dd-auth-wrapper module, is an angularJs 1.x module, used for authentication.
This module wraps [ angular-http-auth ](https://www.npmjs.com/package/angular-http-auth-interceptor), into predefined directives and services.

## Registering the module
You register the module in your app like this:
    var app = angular.module('app', [ 'dd-auth-wrapper' ]);
Or if you are using webpack, you can lazy load the module in your app like this:
    var app = angular.module('app', [ require('dd-auth-wrapper') ]);

## Directive
To enable the module, you need to add the directive to your body tag, with HTML similar to this:
    <body dd-auth-enabled login-section='#login' content-section='#content' class='wait-hidden'>
        <section id='login'>This is my login form....</section>
        <section id='content'>This is my content..</section>
    </body>

The login-section and content-section definitions, defines what part of the HTML should be shown as login, and what part should be shown as content.
Also notice the class 'wait-hidden'. This class is optional and does'nt do anything by itself, but will be removed when the JavaScript is executed. The reason for this, is that you can use it to hide all the content of the page, until the JavaScript is executed, end therefore hide the 'flickering' on page load.