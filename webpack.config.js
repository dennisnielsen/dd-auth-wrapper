const webpack = require('webpack');
const path = require('path');

module.exports = {
    entry: './src/dd-auth-wrapper.js',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'dd-auth-wrapper.min.js'
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.js/,
                exclude: /(node_modules)/,
                loader: 'babel-loader'
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({ jsSHA: 'jssha'}),
        new webpack.optimize.UglifyJsPlugin({
            sourceMap: false,
            compress: {
                warnings: false,
                sequences: true,
                conditionals: true,
                booleans: true,
                if_return: true,
                join_vars: true,
                drop_console: true
            },
            output: {
                comments: false
            },
            minimize: true
        })
    ]
};