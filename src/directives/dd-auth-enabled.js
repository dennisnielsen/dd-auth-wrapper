var $ = require('jquery');

module.exports = function($log) {
    return {
        restrict: 'AC',
        scope: { loginSection: '@', contentSection: '@' },
        link: function(scope, elem) {

            // Checking if mandatory login and content -id is provided.
            if(!scope.loginSection) { $log.warn('login-section is not defined!'); }
            if(!scope.contentSection) { $log.warn('content-section is not defined!'); }

            var login = $(elem).find(scope.loginSection);
            var content = $(elem).find(scope.contentSection);

            // Starting css magic
            content.hide();
            elem.addClass('dsUnauthenticated');

            scope.$on('event:auth-loginRequired', function() {
                elem.removeClass('dsAuthenticated');
                elem.addClass('dsUnauthenticated');
                login.slideDown('slow', function() {
                    content.hide();
                });
            });
            scope.$on('event:auth-loginConfirmed', function() {
                elem.removeClass('dsUnauthenticated');
                elem.addClass('dsAuthenticated');
                content.show();
                login.slideUp();
            });

            elem.removeClass('wait-hidden');
        }
    };
};