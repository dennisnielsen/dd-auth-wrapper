module.exports = function() {
    /**
     * Defines if passwords should be hashed or not.
     * @type {boolean}
     */
    var hashPasswords = false;

    /**
     * Sets the options for SHA
     * @type {object|null}
     */
    var hashOptions = null;

    /**
     * Defines a salt for the hashed value.
     * @type {string|null}
     */
    var hashSecret = null;

    /**
     * Sets url paths for authentications.
     * @type {object|null}
     */
    var urlOptions = null;

    /**
     * Enable password hashing with SHA
     * @param boolean
     * @param options
     */
    this.enablePasswordHash = function(boolean, options) {
        hashPasswords = boolean;
        hashOptions = options;
    };

    /**
     * Sets a salt for the password hash.
     * @param secret
     */
    this.setPasswordSecret = function(secret) {
        hashSecret = secret;
    };

    /**
     * Sets url routes for authentications.
     * @param settings
     */
    this.setUrlOptions = function(settings) {
        urlOptions = settings;
    };

    this.$get = ['ddHttpService', '$sha', '$log', function(ddHttpService, $sha, $log) {

        /**
         * Gets the hashed value, based on configuration option.
         * @param password
         * @returns {*}
         */
        var getSecret = function(password) {
            if(hashPasswords) {
                $sha.setConfig(hashOptions);
                if(hashSecret) {
                    // returns hash with salt
                    return $sha.hmac(password, hashSecret);
                } else {
                    // returns hash without salt
                    return $sha.hash(password);
                }
            } else {
                // returns cleartext
                return password;
            }
        };

        return {
            authenticate: function(username, password) {
                $log.debug('Starting authentication process for login', username);

                var secret = getSecret(password);
                ddHttpService.post(
                    urlOptions.auth.path,
                    {
                        username: username,
                        password: secret
                    })
                    .then(function success(result) {


                    }, function failed(reason) {
                        if(reason.status == 404 || reason.status == 500) {
                            $log.error('Error in authentication request, the service is down!', username);
                        } else {
                            $log.warn('Failed to authenticate user', username);
                        }
                    });
            }
        };
    }];
};