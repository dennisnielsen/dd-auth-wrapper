var angular = require('angular');

module.exports = angular.module('dd-auth-wrapper', [
    require('angular-http-auth'),
    require('angular-sha'),
    require('dd-http-wrapper')
])
    .provider('ddAuthService', require('./services/ddAuthServiceProvider'))
    .directive('ddAuthEnabled', ['$log', require('./directives/dd-auth-enabled')])
    .name;